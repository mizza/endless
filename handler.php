<?php 

	require 'config/config.php';
	require 'classes/DB.php';
	require 'classes/Endless.php';

	if ($_GET['page']){
		$config = [
			'db' => $configDb,
			'firstPageSize' => $_GET['first_page_size'],
			'pageSize' => $_GET['page_size']
		];
		$oEndless = new \endless\Endless($config);
		echo $oEndless->getRecordsInJSON($_GET['page']);
	}


 ?>