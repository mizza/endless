// кол-во записей на странице
PAGE_SIZE = 50;

// кол-во записей на первой странице
FIRST_PAGE_SIZE = 100;

// html-шаблон записи
TEMPLATE = '<section class="record"><div class="image"><img class="record-img" src=""></div><h2 class="record-title"></h2><p class="record-text"></p></section>';