$(function(){
	var endless = new Endless({
		pageSize: PAGE_SIZE,
		firstPageSize: FIRST_PAGE_SIZE,
		template: TEMPLATE
	});
	endless.init();

});