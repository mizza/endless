var Endless = function(params){

	this.pageSize = 50 || params.pageSize;
	this.firstPageSize = 100 || params.firstPageSize;
	this.template = "" || params.template;

	this.page = 1;

	this.loaded = false;

	this.work = true;

	// инициализация работы плагина, обработка события скроллинга и прогрузка первой страницы записей
	this.init = function(){
		// вешаем событие на скролл до низа экрана
		var t = this;
		$(window).scroll(function(){
			if ($(document).height() - $(window).height() <= $(window).scrollTop() + 50) {
				t.loadRecords();
			}
		});
		// прогружаем первую страницу
		t.loadRecords();
	}

	// метод, выполняющий подготовительные действия перед загрузкой записей
	this.beginLoad = function(){
		this.loaded = true;
		$('.wrap > .container').append($('<div class="loader"></div>'));
	}

	// метод, выполняющий завершающие действия после загрузки записей
	this.endLoad = function(){
		this.loaded = false;
		$('.wrap > .container').find('.loader').remove();
		this.page++;
	}

	// ajax-запрос для получения записей
	this.loadRecords = function(){
		if (this.loaded != true && this.work){
			this.beginLoad();
			var t = this;
			$.ajax({
				url: "/handler.php",
				data: "page=" + this.page + "&page_size=" + this.pageSize + "&first_page_size=" + this.firstPageSize,
				success: function(data){
					var records = JSON.parse(data);
					if (records.length == 0){
						t.work = false;
					}else{
						t.appendRecords(records);		
					}
					t.endLoad();
				},
				error: function(data){
					$('.wrap > .container').append($("<p>Во время выполнения запроса произошла непредвиденная ошибка</p>"));
				}
			});
			
		}
	};

	// добавление полученных записей на страницу
	this.appendRecords = function(records){
		for (var i = 0; i < records.length; i++) {
			var recordHtml = $(this.template);
			recordHtml.find(".record-title").text(records[i].title);
			recordHtml.find(".record-text").text(records[i].text);
			recordHtml.find(".record-img").attr('src', "/images/" + this.getImage(i));
			$('.wrap > .container').append(recordHtml);
		};
	}

	// подбор картинки согласно заданию
	this.getImage = function(iter){
		// считаем глобальный индекс записи
		var globalIter = (this.page == 1 ? 0 : (this.page-2)*this.pageSize + this.firstPageSize) + iter;

		if ((iter+1) % 7 == 0){
			return "1.jpg";
		}else if ((iter+1) % 11 == 0){
			return "2.jpg";
		}else if ((iter+1) % 17 == 0){
			return "3.jpg";
		}else{
			var imageIndex = Math.floor(Math.random() * (5 - 4 + 1)) + 4;
			return imageIndex.toString() + ".jpg";
		}
	}
	
	

}