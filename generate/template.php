<!DOCTYPE html>
<html>
<head>
	<title>Генератор записей</title>
	<meta charset="utf-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="../css/style.css">

</head>
<body>
	<div class="wrap">
        <nav id="w0" class="navbar-inverse navbar-fixed-top navbar" role="navigation">
        	<div class="container">
        		<div class="navbar-header">
	        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
	        			<span class="sr-only">Меню</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">Тестовое задание</a>
				</div>
				<div id="w0-collapse" class="collapse navbar-collapse">
					<ul id="w1" class="navbar-nav navbar-right nav"><li><a href="/">Главная</a></li>
						<li><a href="/generate/" class="active">Генерация записей</a></li>
					</ul>
				</div>
			</div>
		</nav>
	        <div class="container">
                <div class="site-index">
				    <div class="body-content">

				        <p>Записей в таблице: <span class="badge"><?=$countRecords?></span></p>

				        <form class="form-inline" method="POST">
						  <div class="form-group">
						    <label>Сгенерировать записей</label>
						    <input type="text" name="generate" class="form-control" value="150">
						  </div>
						  <button type="submit" class="btn btn-success">Сгенерировать</button>
						</form>
						<form class="form-inline" method="POST">
						  <input name="drop" type="submit" class="btn btn-danger" value="Удалить все записи">
						</form>
				    </div>
			</div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <p class="pull-left">© Мызников Павел 2015</p>
        </div>
    </footer>
</body>
</html>
