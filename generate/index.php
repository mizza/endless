	<?php 

		require '../config/config.php';
		require '../classes/DB.php';
		require '../classes/Generator.php';

		$oGenerator = new \generate\Generator($configDb);


		if (isset($_POST['generate'])){
		
			
			$oGenerator->generateRecords(intval($_POST['generate']));
		}
		if (isset($_POST['drop'])){
			$oGenerator->dropRecords();
		}


		$countRecords = $oGenerator->getCountRecords();


		require 'template.php';

	 ?>
	