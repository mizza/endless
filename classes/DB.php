<?php 

namespace db;

class Db{

	protected $link;

	function __construct($configDb) {
       $this->link = $this->connect($configDb);
   	}

   	function __destruct(){
   		$this->disconnect();	
   	}

	private function connect($configDb){
		if ($link = mysqli_connect($configDb['host'],$configDb['user'],$configDb['password'],$configDb['db_name'])){
			return $link;
		}else{
			die("Error " . mysqli_error($this->link)); 
		} 
	}

	private function disconnect(){
		mysqli_close($this->link);
	}

	public function doQuery($str){
		return mysqli_query($this->link, $str);
	}

	public function getQuery($str){
		if ($result = mysqli_query($this->link, $str)){
			$returns = [];
			while ($row = $result->fetch_assoc()) {
		        $returns[] = $row;
		    }
		    return $returns;   
		}else{
			die("Error " . mysqli_error($this->link));
		}
	}

	public function getQueryRow($str){
		if ($result = mysqli_query($this->link, $str)){
			$returns = [];
			if ($row = $result->fetch_row()) {
		    	return $row;   
		    }else{
		    	return false;
		    }
		}else{
			die("Error " . mysqli_error($this->link));
		}
	}

	


}

 ?>