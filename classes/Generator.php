<?php 

namespace generate;

class Generator extends \db\Db{

   	private static $attributives = [ 'Бедный', 'Умный', 'Красивый', 'Сильный', 'Низкий', 'Быстрый'];
   	private static $subjects = [ 'архитектор', 'ученый', 'спортсмен', 'танцор', 'художник', 'программист'];
   	private static $actions = [ 'нарисовал', 'разбил', 'украл', 'продал', 'потерял', 'сделал'];
   	private static $objects = [ 'кружку', 'машину', 'кубик-рубик', 'велосипед', 'клюшку', 'каску'];
   	private static $loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempus congue malesuada. Aliquam volutpat dui sit amet justo mattis, sed tempor nisi lobortis. Nullam interdum vestibulum odio, at molestie tortor laoreet sed. Cras mattis eleifend elit, sit amet consequat enim maximus id. Cras faucibus mi vitae lacinia condimentum. Nunc fermentum augue vitae vulputate varius. Donec sed massa et eros pulvinar pellentesque at eget metus.Nam tempor eros quis velit porta porttitor. Donec malesuada sapien nec odio venenatis, at congue enim efficitur. In eu feugiat lacus. In luctus odio ipsum, et euismod nibh elementum congue. Vestibulum pretium et elit sit amet rhoncus. Duis aliquet ultrices lobortis. Integer sagittis, felis at varius maximus, nisl neque blandit diam, id volutpat diam erat sit amet mauris. Pellentesque finibus tortor nec sollicitudin volutpat. Aliquam metus diam, malesuada quis orci sed, mattis tincidunt tellus. Phasellus tempus et turpis a laoreet. Integer quis felis tincidunt, ornare felis quis, ornare magna. Aenean turpis elit, feugiat sed maximus non, consequat ut justo. Nam et scelerisque velit. Duis tempus rutrum elit ut ullamcorper. Donec massa justo, finibus non faucibus nec, elementum eget est. Donec mauris nisl, pellentesque scelerisque justo id, laoreet tincidunt justo.Cras tincidunt accumsan odio, non consectetur ante faucibus non. Pellentesque ornare imperdiet nisl, vitae porta ex laoreet eleifend. Integer vel dignissim augue. Ut pretium facilisis nibh, quis finibus metus ultrices in. Phasellus euismod felis id dui dapibus vulputate. Nunc in rhoncus metus, eget dictum dolor. Vivamus et sodales magna. Maecenas fringilla ligula odio, non laoreet ante mattis non. Donec sollicitudin orci leo, ac faucibus mi efficitur vel. Nullam vulputate, lorem eu dignissim pharetra, purus neque consequat ligula, quis faucibus lorem eros at mi. Proin semper sagittis ornare. Sed condimentum enim eu lacus lobortis, quis consequat purus tincidunt. Integer sem ipsum, condimentum id magna in, luctus commodo ligula. Nullam aliquet erat vel enim sollicitudin cursus. Curabitur sollicitudin eu massa mattis finibus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque viverra mauris et felis bibendum, at ullamcorper velit fermentum. Morbi justo enim, mollis eget nisi sit amet, rhoncus auctor ipsum. Nullam quis mollis sapien. Nunc scelerisque porta lobortis. Etiam in mi quis massa tristique iaculis. Mauris non ipsum at lorem hendrerit pretium.Nam aliquet, nunc eget euismod tempor, felis odio interdum metus, ut vulputate mi est a erat. Nulla non erat lacus. Ut vel ullamcorper risus. Nunc tempus eros vel magna volutpat, egestas tempus quam fermentum. In hac habitasse platea dictumst. Proin condimentum dolor vitae erat tempor euismod. In dignissim condimentum mi, in cursus turpis congue vitae. Integer hendrerit nibh at viverra luctus. Pellentesque convallis diam dui, at tincidunt urna mattis vitae. Cras varius tincidunt diam id rutrum. Vestibulum ut placerat erat. Curabitur ante enim, tristique a erat sed, rutrum fringilla ipsum. Aliquam eu tellus enim. Sed venenatis orci in iaculis porttitor. Ut lacus massa, mattis in sollicitudin sit amet, accumsan quis tellus.";


	public function getCountRecords(){
		$sql = "SELECT COUNT(*) FROM records";
		$row = $this->getQueryRow($sql);	
		return $row[0];
	}

	public function generateRecords($count){
		for ($i=0; $i < $count; $i++) { 
			$title = self::$attributives[rand(0,5)]." ".self::$subjects[rand(0,5)]." ".self::$actions[rand(0,5)]." ".self::$objects[rand(0,5)];
			$text = mb_substr(self::$loremIpsum, rand(0,2400), rand(470,800));
			$sql = "INSERT INTO records (`title`, `text`) VALUES ('$title', '$text')";
			$this->doQuery($sql);
		}
	}

	public function dropRecords()
	{
		$sql = "TRUNCATE TABLE records";	
		$this->doQuery($sql);
	}
}



 ?>