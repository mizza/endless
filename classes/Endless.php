<?php 

namespace endless;

class Endless extends \db\Db {

	// размер страниц
	private $pageSize;

	// размер первой страницы
	private $firstPageSize;

	function __construct($params){
		parent::__construct($params['db']);
		$this->pageSize = $params['pageSize'];
		$this->firstPageSize = $params['firstPageSize'];
	}

	public function getRecords($page=1)
	{
		$offset = $page == 1 ? 0 : $this->firstPageSize + $this->pageSize*($page-2);
		$limit = $page == 1 ? $this->firstPageSize : $this->pageSize;

		$sql = "SELECT * FROM records LIMIT $offset, $limit";
		return $this->getQuery($sql);
	}

	public function getRecordsInJSON($page=1)
	{
		$records = $this->getRecords($page);
		return json_encode($records);
	}

}


 ?>